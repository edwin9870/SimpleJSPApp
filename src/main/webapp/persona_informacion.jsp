<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.edwin.entidad.Persona"%>
<%@page import="java.util.List"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hello</title>
</head>
<body>

<%
Persona student =(Persona) request.getAttribute("persona");
List<Persona> presidentes = (List<Persona>) request.getAttribute("presidentes");
%>

The full name is: <% out.println(student.getNomber()+" "+student.getApellido()); %>
<br />

<ul>
<%
for(Persona persona: presidentes) {
%>
	<li><% out.print(persona.getNomber()+" "+persona.getApellido()); %></li>
<%}%>
</ul>

</body>
</html>