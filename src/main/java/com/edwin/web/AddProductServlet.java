package com.edwin.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 7082535250718164994L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("GET method ejecutado");
		req.getRequestDispatcher("/new_product.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("POST method ejecutado");

		PrintWriter writer = resp.getWriter();

		writer.println("Product name: " + req.getParameter("name"));
		writer.println("Product price: " + req.getParameter("price"));
		
		writer.close();
	}

}
