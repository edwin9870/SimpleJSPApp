package com.edwin.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/mostrarcookies"})
public class ShowCookie extends HttpServlet{
	private static final long serialVersionUID = 6138301158408882461L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Cookie[] cookies = req.getCookies();
		for(Cookie cookie: cookies) {
			resp.getWriter().println("Nombre cookie: "+cookie.getName() + ", valor: "+ cookie.getValue());
		}
		
	}
	
	

}
