package com.edwin.web.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/agregarsession")
public class CreateSessionServlet extends HttpServlet {
	private static final long serialVersionUID = 2624338300085670977L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		session.setAttribute("nombre", "Pedro");
		
		PrintWriter writer = resp.getWriter();
		
		writer.println("Valor de session creada");
	}
	
	
	
	

}
