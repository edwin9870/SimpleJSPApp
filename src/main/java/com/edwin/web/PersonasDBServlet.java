package com.edwin.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edwin.entidad.Persona;

public class PersonasDBServlet extends HttpServlet {
	private static final long serialVersionUID = -744710204075568991L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter out = res.getWriter();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("klk");
		EntityManager em = emf.createEntityManager();

		List<Persona> personas = em.createNamedQuery("Persona.buscarTodos", Persona.class).getResultList();
		out.println("People's list from database");
		for (Persona persona : personas) {
			out.println("Nombre: " + persona.getNomber()+", apellido: "+ persona.getApellido());
		}

		out.close();
	}

}
