package com.edwin.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/agregarcookie"})
public class CreatingCookieServlet extends HttpServlet {
	private static final long serialVersionUID = -8852879669420614295L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.addCookie(new Cookie("edad", "23"));
		resp.getWriter().println("Cookie agregada!");
	}
	
}
