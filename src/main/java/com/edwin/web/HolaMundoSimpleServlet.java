package com.edwin.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "holaMundoSimpleServlet", urlPatterns = {"/hola2", "/hola1"})
public class HolaMundoSimpleServlet extends HttpServlet {
	private static final long serialVersionUID = -4150924106360071788L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter writer = resp.getWriter();
		writer.append("Hola desde simple servlet");
		
		Object mensajeAttributoSession = req.getSession().getAttribute("mensaje");
		if(mensajeAttributoSession != null) {
			writer.append("Atributo valor: "+ (String)mensajeAttributoSession);
		}
		
		req.getSession().setAttribute("mensaje", "Holaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	}

}
