package com.edwin.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "yondryHelloWorld", urlPatterns = {"/helloWorld"})
public class YondryHelloWorld extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8296318706370052443L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().append("hola mundo!");
	}

}
