package com.edwin.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/attr"})
public class MostrarAtributosSimpleServlet extends HttpServlet {
	private static final long serialVersionUID = -4150924106360071788L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Enumeration<String> attributeNames = req.getSession().getAttributeNames();
		
		while(attributeNames.hasMoreElements()) {
			String attributeName = attributeNames.nextElement();
			String valorAtributo = (String) req.getSession().getAttribute(attributeName);
			resp.getWriter().println(attributeName + " valor: " + valorAtributo);
			
		}
	}

}
