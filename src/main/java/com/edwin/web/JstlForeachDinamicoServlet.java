package com.edwin.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edwin.entidad.Persona;

public class JstlForeachDinamicoServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6704911495365923566L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<com.edwin.entidad.Persona> presidentes = new ArrayList<Persona>();
		
		presidentes.add(new Persona("Fidel", "Castro"));
		presidentes.add(new Persona("Barack", "Obama"));
		presidentes.add(new Persona("Donal", "Trump"));
		presidentes.add(new Persona("Vladimir", "Putin"));
		
		req.setAttribute("presidentes", presidentes);
		req.getRequestDispatcher("/jstl_ejemplos/jstl_foreach_dinamico.jsp").forward(req, resp);
	}
	
	
}

